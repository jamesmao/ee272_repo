#README for EE272

###################################################
##
## Where are the files?
##
###################################################

Say your design name is $design...

Sources for the files are setup under the folder src-$design

These source files should be placed in these folders:
src-$design/rtl          --  This is where your design files go
src-$design/rtl/analog   --  This is where your analog models go for mixed signal
src-$design/rtl/digital  --  This is where your digital modules go for mixed signal
src-$design/verif        --  This is where your validation environment goes
src-$design/primitives   --  This is where you can put "library" type files (e.g., flop, adder, etc.)

Note: 
* The makefile will attempt to compile any .vp file in those folders. 
* The Makefile will consider .vpf files in those folders as list of .vp files to compile


Scripts for synthesis are located in
phys_smpl/scripts
phys_smpl/zrt_scripts

Most management of the flow is done in the Makefile
Makefile

##################################################
##
##  RUNNING THE FLOW
##
##################################################

The flow is broken into multiple steps:

$ tcsh
--> usually the default shell, this shell was tested with the tools.

$ source setup/mod_setup.tcsh 
--> setup the EDA environment

$ make gen 
--> elaborate the design and create *.v files from *.vp using genesis

$ make comp
--> build a simulaton binary from the *.v using vcs

$ make run
--> execute the simulation binary produced by vcs

$ make syn_dc
--> map and size the design in design compiler using topographical mode

$ make comp2
--> build a simulation binary from the gate level netlist using vcs

$ make run2
--> execute the gate level netlist simulation binary produced by vcs
--> produce activity factor extraction for synthesis

$ make syn_dc2
--> resynthesize the design using the new activity factor extractions from gate level netlist

$ make syn_icc
--> run the design through place, route, ..., signoff

Also you can run the whole flow in one pass

$ make gen eval2
--> equivelant to:
$ make gen comp run syn_dc comp2 run2 syn_dc2 syn_icc

$ make cleanall gen eval2
--> is the recommended way to run form scratch


##TODO-> Running for wave
##TODO-> Running dve
##TODO-> Running with Rollup


###################################################################
##
## Makefile Options...
##
###################################################################

$ make gen eval2 DESIGN_NAME=multp
--> runs the designware partial product multiplier

$ make gen eval2 DESIGN_NAME=mult
--> runs the designware full multiplier -currently missing-

$ make gen eval2 DESIGN_NAME=float_mac
--> runs the designware floating point multiply accumulate  -currently missing-

$ make gen eval2 DESIGN_NAME=multp SYN_CLK_PERIOD=1.2
--> runs the partial product multiplier with a clock period or 1.2nS
-->                                     with a unit delay of 1.2nS

$ make gen eval2 DESIGN_NAME=multp GENESIS_CFG_XML=myConfig.xml
--> used myConfig.xml to control the elaboration of the xml files


######################################################################
##
## Adding a new design to the flow:
##
######################################################################

Some variables need to be defined in order to run a new design in this flow:

You can specify these variables everytime on the Makefile.  For example:

make gen eval2             SRC_DIR=src-multp \
                           DESIGN_NAME=multp \
                           INST_NAME=my_multp_inst \
                           MOD_NAME=multp \
                           TOP_NAME=top \
			   VALID_DESIGN_NAME=yes \
 

Additionally, you can add a new entry to the makefile near the other:

  ifeq ($(DESIGN_NAME),strawman01)
    SRC_DIR := genesis-source-StrawMan01
    INST_NAME := my_strawMan01_inst
    MOD_NAME := strawMan01
    TOP_NAME := top
    VALID_DESIGN_NAME := yes
  endif

SRC_DIR -> directory in which your sources can be found
INST_NAME -> name of the instance that will be synthesized
MOD_NAME  -> name of the module that will synthesized
TOP_NAME  -> the name of your top.  For example if top.vp then top 
VALID_DESIGN_NAME -> should be 1 


#TODO exporting parameters from genesis to synthesis...


#################################################################
##
## Running analog design tools
##
#################################################################

The schematic capture and layout tools can be invoked with
$: cdesigner

The circuit simulator can be invoked with:
$: hspice

