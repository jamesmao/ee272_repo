
#####################################
# Brunhaver Reference Flow ee282
# 
##################################### 
# Copied from rm_ -> common_setup
# Copied from reference dir....

source -echo -verbose "temp_config.tcl"

## Check for required variables
if {![info exists DESIGN_NAME]} {
    puts "ERR: MISSING DESIGN_NAME\n" 
    exit 
}
if {![info exists RTL_SOURCE_FILES]} {
    puts "ERR: MISSING \n" 
    exit 
}
if {![info exists SYN_TOP_NAME]} {
    puts "ERR: MISSING SYN_TOP_NAME\n" 
    exit 
}
if {![info exists SYN_INST_NAME]} {
    puts "ERR: MISSING SYN_INST_NAME\n" 
    exit
}

set CONSTRAINTS_INPUT_FILE "design_constraint.tcl"

set_host_options -max_cores 2


##########################################################################################
# Library Setup Variables
##########################################################################################
# To help in finding files in a new delivery setup:
# > cd /global/pilot_support/techlib/montana/saed90nm.02192010/SAED_EDK90nm
# > find . -name \*.db | xargs -l1 dirname | sort -u | less
##########################################################################################

set EDK_ROOT                      [ getenv  "EDK_ROOT" ]
set SYN_DC_ROOT                   [ getenv  "SYNOPSYS" ]

if { ![info exists EDK_ROOT] } {
    puts "Err: Failed to define EDK_ROOT\n"
    exit 
}

if { ![file exists ${EDK_ROOT} ] } {
    puts "Err: Env variable EDK_ROOT points to non-existent directory\n" 
    exit 
}

if { ![info exists SYN_DC_ROOT] } {
    puts "Err: Failed to define SYN_DC_ROOT\n"
    exit 
}

if { ![file exists ${SYN_DC_ROOT} ] } {
    puts "Err: Env variable SYN_DC_ROOT points to non-existent directory\n" 
    exit 
}



set DESIGN_REF_PATH		  "${EDK_ROOT}/90nm_Generic_Libraries_11162011/SAED_EDK90nm"
set DESIGN_REF_NLDM_PATH          "${DESIGN_REF_PATH}/Digital_Standard_cell_Library"
#set DESIGN_REF_RAMS_PATH          "????/Memories"
#set DESIGN_REF_PLL_PATH           "????/Phase_Locked_Loop"
set DESIGN_REF_PADS_PATH          "${DESIGN_REF_PATH}/../SAED_EDK90nm_IO/IO_Standard_Cell_Library"
set DESIGN_REF_TECH_PATH          "${DESIGN_REF_PATH}/Technology_Kit"

puts "EE282 - Assuming RTL location is ../ to current dir ... \n" ;

#set ADDITIONAL_SEARCH_PATH      " \
#        ${DESIGN_REF_NLDM_PATH}/synopsys/models \
#        ${DESIGN_REF_NLDM_PATH}/synopsys/models/clock_gating \
#        ${DESIGN_REF_NLDM_PATH}/synopsys/models/level_shifters \
#        ${DESIGN_REF_NLDM_PATH}/synopsys/models/retention \
#        ${DESIGN_REF_NLDM_PATH}/synopsys/models/retention/with_async_reset \
#        ${DESIGN_REF_RAMS_PATH}/synopsys/models \
#	${DESIGN_REF_PLL_PATH}/synopsys \
#	${DESIGN_REF_PADS_PATH}/synopsys/models \
#       [file normalize [file join [pwd] {../}]] \
#      /hd/cad/synopsys/dc_shell/F-2011.09/dw/sim_ver \
#       "
set ADDITIONAL_SEARCH_PATH      " \
        ${DESIGN_REF_NLDM_PATH}/synopsys/models \
        ${DESIGN_REF_NLDM_PATH}/synopsys/models/clock_gating \
        ${DESIGN_REF_NLDM_PATH}/synopsys/models/level_shifters \
        ${DESIGN_REF_NLDM_PATH}/synopsys/models/retention \
        ${DESIGN_REF_NLDM_PATH}/synopsys/models/retention/with_async_reset \
        [file normalize [file join [pwd] {./scripts/}]] \
        [file normalize [file join [pwd] {./zrt_scripts/}]] \
        [file normalize [file join [pwd] {./../}]] \
        ${SYN_DC_ROOT}/dw/sim_ver \
        ${SYN_DC_ROOT}/libraries/syn/ \
        "

#------------------------------------------------
# STD-CELL LIB NAMES BY OPCOND:
# File_name           Op_cond   Volt  Temp tree_type
# nominal --------------------> 1.20   25
# saed90nm_max*       WORST     0.70  125  worst_case_tree    
# saed90nm_max_hth*   WORST     1.08  125  worst_case_tree    synthesis MAX lib
# saed90nm_min*       BEST      1.32  -40  best_case_tree     synthesis MIN lib
# saed90nm_min_ltl*   BEST      0.90  -40  best_case_tree
#------------------------------------------------
#set TARGET_LIBRARY_FILES    " \
#        saed90nm_max_hth.db \
#        saed90nm_max_hth_hvt.db \
#        saed90nm_max_hth_lvt.db \
#	saed90nm_max_hth_cg.db \
#	saed90nm_max_hth_cg_hvt.db \
#	saed90nm_max_hth_cg_lvt.db \
#	saed90nm_lsh_max_hth.db \
#	saed90nm_lsh_max_hth_hvt.db \
#	saed90nm_lsh_max_hth_lvt.db \
#	saed90nm_max_hth_rd.db \
#	saed90nm_max_hth_rd_hvt.db \
#	saed90nm_max_hth_rd_lvt.db \
#       "
#TODO: Change to typical
set TARGET_LIBRARY_FILES    " \
        saed90nm_typ.db 
        "

#------------------------------------------------
# RAMS_GTS LIB NAMES BY OPCOND:
# File_name           Op_cond   Volt  Temp 
# nominal --------------------> 1.20   25
# saed90nm_max_low_v* WORST     0.70  125     
# saed90nm_max*       WORST     1.08  125     synthesis MAX lib
# saed90nm_min*       BEST      1.32  -40     synthesis MIN lib
# saed90nm_min_low_v* BEST      0.90  -40  
#------------------------------------------------
#set ADDITIONAL_LINK_LIB_FILES     " \
#	SRAM22x32_max.db \
#	SRAM39x32_max.db \
# 	SRAM32x64_max.db \
#	SRAM32x256_1rw_max.db \
#	SRAM8x1024_1rw_max.db  \
# 	"

#set ADDITIONAL_LINK_LIB_FILES     " \
#	${ADDITIONAL_LINK_LIB_FILES} \
#	PLLwc.db \
#	"

#------------------------------------------------
# PADS LIB NAMES BY OPCOND:
# File_name           Op_cond   Volt  Temp 
# nominal --------------------> 1.20   25
# saed90nm_io_max*    WORST     1.08  125     
# saed90nm_io_min*    BEST      1.32  -40     
# saed90nm_io_typ*    TYPICAL   1.20   25     
#------------------------------------------------
#set ADDITIONAL_LINK_LIB_FILES     " \
#	${ADDITIONAL_LINK_LIB_FILES} \
#	saed90nm_io_max_fc.db \
#  	"
set ADDITIONAL_LINK_LIB_FILES ""

#------------------------------------------------
# lib to use for MAX delay | lib to use for MIN delay
#------------------------------------------------
#set MIN_LIBRARY_FILES   " \
#        saed90nm_max_hth.db     	saed90nm_min.db \
#        saed90nm_max_hth_hvt.db 	saed90nm_min_hvt.db \
#        saed90nm_max_hth_lvt.db 	saed90nm_min_lvt.db \
#	saed90nm_max_hth_cg.db 		saed90nm_min_cg.db \
#	saed90nm_max_hth_cg_hvt.db 	saed90nm_min_cg_hvt.db \
#	saed90nm_max_hth_cg_lvt.db 	saed90nm_min_cg_lvt.db \
#	saed90nm_lsh_max_hth.db 	saed90nm_lsh_min.db \
#	saed90nm_lsh_max_hth_hvt.db 	saed90nm_lsh_min_hvt.db \	
#	saed90nm_lsh_max_hth_lvt.db 	saed90nm_lsh_min_lvt.db \
#	saed90nm_max_hth_rd.db 		saed90nm_min_rd.db \
#	saed90nm_max_hth_rd_hvt.db 	saed90nm_min_rd_hvt.db \
#	saed90nm_max_hth_rd_lvt.db 	saed90nm_min_rd_lvt.db \
#	SRAM22x32_max.db                SRAM22x32_min.db \
#	SRAM39x32_max.db                SRAM39x32_min.db \
#	SRAM32x64_max.db                SRAM32x64_min.db \
#	SRAM32x256_1rw_max.db           SRAM32x256_1rw_min.db \
#	SRAM8x1024_1rw_max.db           SRAM8x1024_1rw_min.db \
#	PLLwc.db 			PLLbc.db \
#	saed90nm_io_max_fc.db		saed90nm_io_min_fc.db \
#       "
#set MIN_LIBRARY_FILES   " \
#        saed90nm_max_hth.db     	saed90nm_min.db \
#        saed90nm_max_hth_hvt.db 	saed90nm_min_hvt.db \
#        saed90nm_max_hth_lvt.db 	saed90nm_min_lvt.db \
#	saed90nm_max_hth_cg.db 		saed90nm_min_cg.db \
#	saed90nm_max_hth_cg_hvt.db 	saed90nm_min_cg_hvt.db \
#	saed90nm_max_hth_cg_lvt.db 	saed90nm_min_cg_lvt.db \
#	saed90nm_max_hth_rd.db 		saed90nm_min_rd.db \
#	saed90nm_max_hth_rd_hvt.db 	saed90nm_min_rd_hvt.db \
#	saed90nm_max_hth_rd_lvt.db 	saed90nm_min_rd_lvt.db \
#        "
set MIN_LIBRARY_FILES    " \
  saed90nm_max_nth.db saed90nm_min_nt.db \
  saed90nm_max_hth.db saed90nm_min_ht.db \
  saed90nm_max_lth.db saed90nm_min.db \
  "

#set MW_REFERENCE_LIB_DIRS  " \
#	${DESIGN_REF_NLDM_PATH}/process/astro/fram/saed90nm_fr \
#	${DESIGN_REF_NLDM_PATH}/process/astro/fram/saed90nm_hvt_fr \
#	${DESIGN_REF_NLDM_PATH}/process/astro/fram/saed90nm_lvt_fr \
#       ${DESIGN_REF_RAMS_PATH}/process/astro/saed_sram_fr \
#        ${DESIGN_REF_PLL_PATH}/process/astro/PLL_fr \
#        ${DESIGN_REF_PADS_PATH}/process/astro/saed_io_fc_fr \
#	"
set MW_REFERENCE_LIB_DIRS  " \
	${DESIGN_REF_NLDM_PATH}/process/astro/fram/saed90nm"
#	${DESIGN_REF_NLDM_PATH}/process/astro/fram/saed90nm_hvt \
#	${DESIGN_REF_NLDM_PATH}/process/astro/fram/saed90nm_lvt \
#	"

set MW_REFERENCE_CONTROL_FILE     ""

set TECH_FILE        "${DESIGN_REF_TECH_PATH}/techfile/saed90nm_icc_1p9m.tf"
set MAP_FILE         "${DESIGN_REF_TECH_PATH}/rules/starrcxt/tech2itf.map"
set TLUPLUS_MAX_FILE "${DESIGN_REF_TECH_PATH}/rules/starrcxt/tluplus/saed90nm_1p9m_1t_Cmax.tluplus"
set TLUPLUS_MIN_FILE "${DESIGN_REF_TECH_PATH}/rules/starrcxt/tluplus/saed90nm_1p9m_1t_Cmin.tluplus"


set MW_POWER_NET                "VDD" ;#
set MW_POWER_PORT               "VDD" ;#
set MW_GROUND_NET               "VSS" ;#
set MW_GROUND_PORT              "VSS" ;#

set MIN_ROUTING_LAYER            "M1"   ;# Min routing layer
set MAX_ROUTING_LAYER            "M8"   ;# Max routing layer

##########################################################################################
# Multi-Voltage Common Variables
#
# Define the following MV common variables for the RM scripts for multi-voltage flows.
# Use as few or as many of the following definitions as needed by your design.
##########################################################################################

set PD1                          ""           ;# Name of power domain/voltage area  1
set PD1_CELLS                    ""           ;# Instances to include in power domain/voltage area 1
set VA1_COORDINATES              {}           ;# Coordinates for voltage area 1
set MW_POWER_NET1                "VDD1"       ;# Power net for voltage area 1
set MW_POWER_PORT1               "VDD"        ;# Power port for voltage area 1

set PD2                          ""           ;# Name of power domain/voltage area  2
set PD2_CELLS                    ""           ;# Instances to include in power domain/voltage area 2
set VA2_COORDINATES              {}           ;# Coordinates for voltage area 2
set MW_POWER_NET2                "VDD2"       ;# Power net for voltage area 2
set MW_POWER_PORT2               "VDD"        ;# Power port for voltage area 2

set PD3                          ""           ;# Name of power domain/voltage area  3
set PD3_CELLS                    ""           ;# Instances to include in power domain/voltage area 3
set VA3_COORDINATES              {}           ;# Coordinates for voltage area 3
set MW_POWER_NET3                "VDD3"       ;# Power net for voltage area 3
set MW_POWER_PORT3               "VDD"        ;# Power port for voltage area 3

set PD4                          ""           ;# Name of power domain/voltage area  4
set PD4_CELLS                    ""           ;# Instances to include in power domain/voltage area 4
set VA4_COORDINATES              {}           ;# Coordinates for voltage area 4
set MW_POWER_NET4                "VDD4"       ;# Power net for voltage area 4
set MW_POWER_PORT4               "VDD"        ;# Power port for voltage area 4
