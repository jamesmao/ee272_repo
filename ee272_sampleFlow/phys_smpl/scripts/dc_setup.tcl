
#####################################
# Brunhaver Reference Flow ee282
# 
##################################### 

puts "RM-Info: Running script [info script]\n"

source -echo -verbose scripts/common_setup.tcl
source -echo -verbose scripts/dc_setup_filenames.tcl

# Portions of dc_setup.tcl may be used by other tools so do check for DC only commands
if {$synopsys_program_name == "dc_shell"}  {

  # Change alib_library_analysis_path to point to a central cache of analyzed libraries
  # to save some runtime and disk space.  The following setting only reflects the
  # the default value and should be changed to a central location for best results.

  set alib_library_analysis_path .

  # Add any additional DC variables needed here
}

set RTL_SOURCE_FILES  ""      ;# Enter the list of source RTL files if reading from RTL

# The following variables are used by scripts in dc_scripts to direct the location
# of the output files

set REPORTS_DIR "reports"
set RESULTS_DIR "results"

file mkdir reports
file mkdir results


#################################################################################
# Library Setup
#
# This section is designed to work with the settings from common_setup.tcl
# without any additional modification.
#################################################################################

# Define all the library variables shared by all the front-end tools
# The "file normalize" converts the relative paths to absolute

set_app_var search_path ${ADDITIONAL_SEARCH_PATH}
#set_app_var search_path "$search_path [file normalize [file join [pwd] {./scripts}]]"
#set_app_var search_path "$search_path [file normalize [file join [pwd] {./..}]]"
puts "These: \n ${ADDITIONAL_SEARCH_PATH} \n " 

# Milkyway variable settings

# Make sure to define the following Milkyway library variables
# mw_logic1_net, mw_logic0_net and mw_design_library are needed by write_milkyway

set mw_logic1_net ${MW_POWER_NET}
set mw_logic0_net ${MW_GROUND_NET}

set mw_reference_library ${MW_REFERENCE_LIB_DIRS}
set mw_design_library ${DESIGN_NAME}_LIB

set mw_site_name_mapping [list CORE unit Core unit core unit]

# miscellaneous setups
set report_default_significant_digits 3
set sh_enable_page_mode false

# The remainder of the setup below should only be performed in Design Compiler
if {$synopsys_program_name == "dc_shell"}  {

  # Include all libraries for multi-Vth leakage power optimization

  set target_library ${TARGET_LIBRARY_FILES}
  set synthetic_library dw_foundation.sldb
  set link_library "* $target_library $ADDITIONAL_LINK_LIB_FILES $synthetic_library"

  # Set min libraries if they exist
  foreach {max_library min_library} $MIN_LIBRARY_FILES {
    set_min_library $max_library -min_version $min_library
  }

  if {[shell_is_in_topographical_mode]} {

    # Only create new Milkyway design library if it doesn't already exist
    if {![file isdirectory $mw_design_library ]} {
      create_mw_lib   -technology $TECH_FILE \
                      -mw_reference_library $mw_reference_library \
                      $mw_design_library
    } else {
      # If Milkyway design library already exists, ensure that it is consistent with specified Milkyway reference libraries
      set_mw_lib_reference $mw_design_library -mw_reference_library $mw_reference_library
    }

    open_mw_lib     $mw_design_library

    check_library

    set_tlu_plus_files -max_tluplus $TLUPLUS_MAX_FILE \
                       -min_tluplus $TLUPLUS_MIN_FILE \
                       -tech2itf_map $MAP_FILE

    check_tlu_plus_files

    # Add any IC Compiler ILMs to the link library.
    # If you are using IC Compiler ILMs in Design Compiler wireload mode, you will need
    # to use the Milkyway library setup in Design Compiler wireload mode as well.
    # In that case, remove the "if {[shell_is_in_topographical_mode]}"
    # to include this Milkyway library setup in Design Compiler wireload mode.

  }

  #################################################################################
  # Library Modifications
  #
  # Apply library modifications here after the libraries are loaded.
  #################################################################################

  # source dont_use.tcl
  
  # set bus_naming_style "%s_%d"

  #source my-verilog.name_rule.tcl
  # report_name_rule my-verilog
  # report_name_rule verilog

  #source dc_tech-saed90nm.tcl
} 

# Most of these are due to the loose RTL coding style and sloppiness.
suppress_message {ELAB-294} ;# Warning: Floating pin ... connected to ground
suppress_message {ELAB-311} ;# Warning: DEFAULT branch of CASE statement cannot be reached
suppress_message {ELAB-349} ;# Warning: Potential simulation-synthesis mismatch if index exceeds size of array
suppress_message {ELAB-802} ;# Warning: The port default value in entity declaration ... is not supported
suppress_message {ELAB-832} ;# Warning: Undriven register ... is connected to primary output ...
suppress_message {ELAB-833} ;# Warning: Register ... is undriven
suppress_message {ELAB-924} ;# Warning: Signal assignment delays are not supported for synthesis

# propagate ties as if they were cases.
# this is needed to block conditional Async Write-thru paths in the RAMs
set case_analysis_with_logic_constants true

puts "RM-Info: [info script] Finished\n"


# end
