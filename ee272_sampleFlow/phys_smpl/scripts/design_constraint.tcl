
#####################################
# Brunhaver Reference Flow ee282
# 
##################################### 


##SETUP SOME LOCAL ALIASES
set CLK clk
set RST reset 

##TODO: Configure from Makefile
set CLK_PERIOD ${SYN_CLK_PERIOD}

##Set Wire Load Model  ##NOT USED IN TOPO 
#set_wire_load_model -name zero_wlm

## Sample Config for PipeLined Design
#create_clock $CLK -period $CLK_PERIOD
#set_output_delay [ expr $CLK_PERIOD*1/2 ] -clock $CLK  [get_ports "*" -filter {@port_direction == out} ]
#set all_inputs_wo_rst_clk [remove_from_collection [remove_from_collection [all_inputs] [get_port $CLK]] [get_port $RST]]
#set_input_delay -clock $CLK [ expr $CLK_PERIOD*1/2 ] $all_inputs_wo_rst_clk

## Constraint for Delay Based Evaluation
set_max_delay -from [all_inputs] -to [all_outputs] $CLK_PERIOD

## Constraint for Delay Based Evaluation using Clock
create_clock $CLK -period $CLK_PERIOD
set_output_delay 0 -clock $CLK [get_ports "*" -filter {@port_direction == out} ]
set all_inputs_wo_rst_clk [remove_from_collection [remove_from_collection [all_inputs] [get_port $CLK]] [get_port $RST]]
set_input_delay -clock $CLK 0 $all_inputs_wo_rst_clk

#TODO: Provide RST Constraints

## Optimization Flags
#set_leakage_optimization true
#set_dynamic_optimization true




