proc Sm_set_Synopsys_variables {} {
    global env
    global Sm_max_transition 
    global Sm_out_load
    global Sm_max_fanout
    global target_library
    global Sm_synopsys_shell
    global Sm_driving_cell
    global Sm_driving_pin

    # For quicker synthesis
    if { $Sm_synopsys_shell != "pt_shell" } {
#    set ::compile_use_low_timing_effort true 
    set ::hdlin_auto_save_templates true
    set_ultra_optimization true ;# DC Ultra features 
    set ::dw_prefer_mc_inside true ;# MC inside DC 
   }
    set ::auto_wire_load_selection false

    set Sm_max_fanout 16

 # Read the library so that the load_of command will work
    redirect ./error {set libs_read [get_libs *] }

    regsub {\.db} $::target_library {} tlib_ndb

    if { [llength $libs_read ] !=  0 }   {
	       if {[lsearch -regexp [collection_to_list $libs_read ] $tlib_ndb] == -1 } {
	           read_db $target_library 
	       }
    } else {
   # Subbu - added reset on check_error as get_libs * command errors out
   #  with UID-109 : "The object of the specified type could not be found"
	# Sergio : check_error doesn't work in Primetime
	if { $Sm_synopsys_shell != "pt_shell" } {
             echo "Check Error is being reset"
             check_error -v -reset 
	}
	     read_db $target_library 

    }

    set Sm_max_transition 1.2
    set Sm_driving_cell   INVX1
    set Sm_driving_pin    Y
    set Sm_out_load [expr 10*[load_of $tlib_ndb/INVX1/A]]
    
}

proc extract_dotf_lists {filename xvloglist xinclist xmcllist xmclh_list} {
    global env

    upvar 1 $xvloglist verilogfilelist
    upvar 1 $xinclist incdirlist
    upvar 1 $xmcllist mclfilelist
    upvar 1 $xmclh_list mclh_list

    set verilogfilelist ""
    set incdirlist ""
    set mclfilelist ""

    set rtlfiles_f [open $filename r]
    set rtlfiles_list [split [read -nonewline $rtlfiles_f] \n]
    close $rtlfiles_f


    foreach line $rtlfiles_list {
        set the_line $line
	# remove comments
        regsub {\/\/.*} $line {} line

        # remove leading line spaces
        regsub {^\s*} $line {} line

        # remove line ending line spaces
        regsub {\s*$} $line {} line

	# skip empty lines
	if { [regexp -expanded {^\s*$} $line] } {
	    continue
	}

	# Should not be any lines with -v in them 
	if { [regexp -expanded {\-v} $line] } {
	    puts "Sm_Error: $the_line contains a -v and it should not."
	    continue
	}

	# Should not be any lines with -y in them 
	if { [regexp -expanded {\-y} $line] } {
	    puts "Sm_Error: $the_line contains a -y and it should not."
	    continue
	}

	# Should not be any lines with -v in them 
	if { [regexp -expanded {\-f} $line] } {
	    error "$the_line contains a -f and it should not."
	}

        # I don't know if we should actually have libext in .f files so for now I will error.
        if { [regexp libext $line] } {
	    puts "Sm_Error: $the_line contains a libext and it should not."
	    continue 
        }

        # replace environment variables
        if {[ regexp -expanded {\$([A-Z_]+)} $line match submatch ]} {
            regsub {\$[A-Z_]+} $line "$env($submatch)" line
        }

        # check that all $ have been replaced
        if {[ regexp -expanded {\$} $line ]} {
	    error "$the_line still contains a $ after environment variable substitution."
        }

        # handle incdirs
        if { [regexp {\+incdir\+} $line]  } {
            regsub {\+incdir\+} $line {} line
            if {[file isdirectory $line]} {
                lappend incdirlist $line
                continue
            } else {
                error "directory $line pointed to by incdir in $the_line does not exist"
            }
        }

        
        # handle .v files
        if { [regexp {\.v} $line]  } {
            if {[file exists $line]} {
                lappend verilogfilelist $line
                continue
            } else {
                error "verilog file $line  pointed to in $the_line does not exist"
            }
        }

        # handle .mcl files
        if { [regexp {\.mcl} $line]  } {
            if {[file exists $line]} {
                lappend mclfilelist $line
                continue
            } else {
                error "mcl file $line pointed to in $the_line does not exist"
            }
        }

        error "This line does not make sense $the_line"
    }
    regsub -all { } $incdirlist {:} mclh_list
    echo "Done of foreach loop\n"
    echo "Incdir list = $incdirlist\n"
    echo "verilog file list = $verilogfilelist\n"
    echo "mcl file list = $mclfilelist\n"
#    keylset res INCDIRS $incdirlist
#    keylset res VERILOGFILES $verilogfilelist
#    keylset res MCLFILES $mclfilelist
#    set res(INCDIRS) $incdirlist
#    set res(VERILOGFILES) $verilogfilelist
#    set res(MCLFILES) $mclfilelist
#    return $res
}

proc dotvf2filelist {filename} {
    global env

    set filelist ""

    set rtlfiles_f [open $filename r]
    set rtlfiles_list [split [read -nonewline $rtlfiles_f] \n]
    close $rtlfiles_f

    foreach file $rtlfiles_list {
	# skip empty lines
	if { [regexp -expanded {^\s*$} $file] } {
	    continue
	}
	# skip comment lines
	if { [regexp -expanded {\/\/} $file] } {
	    continue
	}
	regsub {\$SMASH} $file "$env(SMASH)" file
	regsub {\$PROCESSOR_HOME} $file "$env(PROCESSOR_HOME)" file
	regsub {\-v} $file {} file
	if { [regexp incdir $file]  } {
	    continue
	}
	if { ! [file exists $file] } {
	    error "$file does not exist"
	} else {
	    lappend filelist $file
	}
    }
    
    return $filelist
}


proc collection_to_list {a_collection} {
  set object_list {}
  foreach_in_collection a_object $a_collection {
      set name [get_attribute $a_object full_name]
      # Certain collection objects may not have full_name attribute
      if {$name == ""} {
	  set name [get_attribute $a_object name]
      }
      lappend object_list $name
  }
  return $object_list
}

# Delete an element from a list and return a new list
# Stolen from Brent Welch book
proc ldelete { list value } {
        set ix [lsearch -exact $list $value]
    if {$ix >= 0} {
                return [lreplace $list $ix $ix]
    } else {
                return $list
    }
}

#
#####################################################################
# author: TO
# birth: 10/16/2001
# update: 
# desc: This script combines report_hier and report_reference 
#       It creates a partial hierarchy report which shows the area
#       and overall percent contribution to the top design area
#
#####################################################################
proc profile_area {args} {

set space ""
set tmpfile .profile_report123
sh rm -f $tmpfile
set hierfile .profile_hier123
sh rm -f $hierfile

set results(-level) 2
parse_proc_arguments -args $args results

set level $results(-level)

redirect /dev/null { set rcs_string {$Id: profile_area.proc,v 1.4 2002/01/07 22:45:45 xxxxx Exp $} }
redirect /dev/null { set version [lindex $rcs_string 2]  }

echo "Running Profile Tool version $version"
echo "--------------------------------------"

set wildcard "*"
set back ".."

redirect $tmpfile {echo "level_is 0"}
redirect $tmpfile -append {report_reference -nosplit}
redirect $hierfile {report_hier -nosplit}

for { set lev 1 } { $lev < $level } { incr lev } {
     redirect /dev/null {set hier_cells [get_cell $wildcard -filter "@is_hierarchical==true"]}
     foreach_in_collection hc $hier_cells {
         set hc_name [get_object_name $hc]
         redirect /dev/null {current_instance $hc_name}
         redirect $tmpfile -append {echo "level_is $lev"}
         redirect $tmpfile -append {report_reference -nosplit}
         redirect /dev/null {current_instance $back}
     }
     set wildcard "${wildcard}/*"
     set back "${back}/.."
}



set outstring ""
set levelnum 0

if [file exists $tmpfile] {
  set tmp_reports [open $tmpfile r]
    while {[gets $tmp_reports line] >= 0} {
	if [regexp {Design : (.*)} $line full parent] {
	    if [regexp {([^ ]+) \(([^ ]+)\)} $parent toss inst parent] { } 
	    set prims_tot($parent) 0
	} elseif [regexp {^(\S+)\s+([0-9\.]+)\s+(\d+)\s+([0-9\.]+)\s+([a-z, ]+)} \
		$line full ref unit num total attr] {
	    if [regexp {h|h,} $attr match]  {
		set area($ref) [expr $total]
	    } else {
		set prims_tot($parent) [expr $prims_tot($parent) + $total]
	    }
	} elseif [regexp {Total [0-9]+ references[ ]+([0-9\.]+)} $line full total] {
	  set area($parent) [expr $total]
	} elseif [regexp {level_is ([0-9]+)} $line full levelnum] {
	}
        

    }
}

set tot_area 0
if [file exists $hierfile] {
  set hier_reports [open $hierfile r]
  while {[gets $hier_reports line] >= 0} {
      if [regexp {^\*\*\*\*} $line tmp] {
      } elseif [regexp {([ ]*)([^ ]+)} $line full space ref] {
	  if { [regexp {lmap_top} $ref] } {
	      puts "$line, $full, $space, $ref, $tot_area, $area($ref)"
	  }
          if {[array names area $ref]!=""} {
             if { $tot_area == 0 } {
                 set tot_area $area($ref)
                 echo [format \
                     "%[string length $space]s%-35s%[expr 20-[string length $space]]s %13s    %7s" \
                     $space "Reference" " " "Area" "Percent Overall"]
             }
             set perc [expr (1 - ($tot_area - $area($ref))/$tot_area)*100]
             echo [format \
                    "%[string length $space]s%-35s%[expr 20-[string length $space]]s  %12.1f     %5.1f%%" \
                    $space $ref " " $area($ref) $perc]
          }
      } 
  }
}


close $hier_reports
close $tmp_reports
}


define_proc_attributes profile_area \
  -info "Report of the hierarchical area" \
  -define_args {
    { -level   "Number of levels to report"         ""          int  optional }
  }

# ungroup.tcl - DC-Tcl proc to ungroup DW or non-DW cells
# chrispy@synopsys.com
#
# 6/10/99 initial release
# 1/11/01 added some attributes for robustness

proc ungroup_dw {args} {
  global sh_dev_null

  set results(-all_but_dw) 0
  set results(-hierarchical) 0
  parse_proc_arguments -args $args results

  set all_hier_cells [get_cells * -filter "@is_hierarchical == true"]
  set dw_cells ""
  set nondw_cells ""
  set nondw_designs ""

  # make a list of DW, non-DW cells
  foreach_in_collection this_cell $all_hier_cells {
    set this_design [get_designs [get_attribute -quiet $this_cell ref_name]]
    if {[string match "true" [get_attribute -quiet $this_design DesignWare]] ||
        [string match "true" [get_attribute -quiet $this_design mc_design]]} {
      if {![string match "true" [get_attribute -quiet $this_design is_sequential]]} {
        set dw_cells [add_to_collection $dw_cells $this_cell]
      }
    } else {
      set nondw_cells [add_to_collection $nondw_cells $this_cell]
      set nondw_designs [add_to_collection -unique $nondw_designs $this_design]
    }
  }

  redirect $sh_dev_null {set orig_design [current_design]}

  # first, go into any non-DW designs if we're doing hierarchical ungrouping
  if {$results(-hierarchical)} {
    foreach_in_collection this_design $nondw_designs {
      redirect $sh_dev_null {current_design $this_design}
      switch $results(-all_but_dw) {
        0 {ungroup_dw -hier}
        1 {ungroup_dw -hier -all_but_dw}
      }
    }

    redirect $sh_dev_null {current_design $orig_design}
  }

  # now ungroup what we need to at this level
  switch $results(-all_but_dw) {
    0 {
      foreach_in_collection this_cell $dw_cells {
        echo "Ungrouping DW cell [get_object_name $this_cell] in [get_object_name $orig_design]..."
      }
      if {$dw_cells != ""} { ungroup -flatten $dw_cells }
    }
    1 {
      foreach_in_collection this_cell $nondw_cells {
        echo "Ungrouping non-DW cell [get_object_name $this_cell] in [get_object_name $orig_design]..."
      }
      if {$nondw_cells != ""} { ungroup $nondw_cells }
    }
  }
}

define_proc_attributes ungroup_dw \
  -info "ungroup DesignWare or non-DesignWare cells" \
  -define_args \
  {
    {-all_but_dw "ungroup non-DW hierarchy cells" "" boolean optional}
    {-hierarchical "perform operation on subhierarchy as well" "" boolean optional}
  }

proc Sm_change_name_rules {} {
    change_names -rules vcad_top_rules -verbose -hierarchy 
    change_names -rules vcad_core_rules -verbose -hierarchy 
    change_names -rules vcad_net_rules -verbose -hierarchy 
    change_names -rules vcad_cell_rules -hier
    change_names -rules verilog -hier 
    set ::verilogout_show_unconnected_pins true 
    set ::verilogout_no_tri true 
    set ::verilogout_higher_designs_first true 
    set_fix_multiple_port_nets -all
    set_auto_disable_drc_nets -constant false
 }

proc SM_reg_count {} {
    echo "Starting Register Count ..."
    echo "====================================================="   
    global current_design
    foreach_in_collection design [ get_designs "*" ] {
	current_design $design
	set dsg $current_design
	echo "$dsg has [sizeof_collection [all_registers -edge_triggered]] flops"
	echo "$dsg has [sizeof_collection [all_registers -level_sensitive]] latchs"
	echo "$dsg has [sizeof_collection [all_registers]] total registers"
	echo "====================================================="   
    }
    echo "Ending Register Count ..."
}	

proc SM_scan_stitch {} {


    set_dft_signal -view existing_dft -type ScanClock -port Clk -timing {50 100}
    set_dft_signal -view existing_dft -type Reset -port Reset -active_state 1
    set_dft_signal -view existing_dft -type ScanEnable -port ScanEn -active_state 1
    set_dft_signal -view existing_dft -type TestMode -port TMode -active_state 1
    

}	

proc SM_fix_netlist {} {
	#Need to remove the don't touch attribute. and run compile -drc 
	remove_unconnected_ports  [get_cell -hier] 
	set_fix_multiple_port_nets -buffer_constants -all [get_designs *]
	set hdlout_internal_busses true
	change_names -rules verilog -hier -log_changes name_change.log -verbose
	set ::verilogout_show_unconnected_pins true 
	set ::verilogout_no_tri true 
	set ::verilogout_higher_designs_first true 
}	

proc SM_write_gv {} {
    global design
    global sscript
    current_design $design 
    define_name_rules verilog -remove_internal_net_bus -flatten_multi_dimension_busses -check_bus_indexing -remove_irregular_net_bus
    change_names -rules verilog -hierarchy
    write -f verilog -h -o net/$design.$sscript.gv
}	

