
# TSMC 45nm
source $env(HOME)/synopsys_startup/scr_tsmc45/common_options.tcl
source $env(HOME)/synopsys_startup/scr_tsmc45/libsetup.tcl -continue_on_error

# ST 90nm
#source $env(HOME)/synopsys_startup/scr_ST90/common_options.tcl
#source $env(HOME)/synopsys_startup/scr_ST90/libsetup.tcl -continue_on_error
