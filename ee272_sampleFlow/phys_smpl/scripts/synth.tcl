
#####################################
# Brunhaver Reference Flow ee282
# 
##################################### 

# Source Setup Scripts
source -echo -verbose scripts/common_setup.tcl
source -echo -verbose scripts/dc_setup.tcl 

## Define the verification setup file for Formality
set_svf results/${DESIGN_NAME}.mappped.svf

## Include an RTL SAIF for better power optimization and analysis.
if { [file exists ${SYN_SAIF_FILE}] } {
  saif_map -start
}

## Read in the RTL Design
puts "EE282-Info: Analyzing and Elaborating ${DESIGN_NAME}\n"
define_design_lib WORK -path ./WORK
analyze -format sverilog [glob ../*unq*.v]
elaborate ${DESIGN_NAME} -architecture verilog 

## Check design for eRRors
puts "EE282-Info: Checking Design\n"
check_design -summary
check_design

## Design Constraints
puts "EE282-Info: Sourcing script file [which ${CONSTRAINTS_INPUT_FILE}]\n"
source -echo -verbose ${CONSTRAINTS_INPUT_FILE}

## Power Estimation
if { [file exists ${SYN_SAIF_FILE}] } {
  read_saif -auto_map_names -instance ${SYN_TOP_NAME}/${SYN_INST_NAME} -input ${SYN_SAIF_FILE} -verbose
  set_power_prediction true  
}

## Sample Naive Pipeline Retiming Commands		
set_optimize_registers true -design ${DESIGN_NAME}
compile_ultra -no_autoungroup -retime

## Map RTL to Gate Level Netlist and Size Standard Cells (no-Retime)
#compile_ultra -no_autoungroup

# Write and close SVF file and make it available for immediate use
set_svf -off

#Write out some result files
write -format verilog -hierarchy -output results/$DESIGN_NAME.mapped.v
write -format ddc -hierarchy -output results/$DESIGN_NAME.mapped.ddc
write_sdc -nosplit results/$DESIGN_NAME.mapped.sdc

file mkdir reports

#Write out a design Check
check_design -summary
check_design
check_design > reports/${DESIGN_NAME}.mapped.check_design.rpt

#Write out some reports
report_timing -significant_digits 8 -transition_time -nets -attributes -nosplit > reports/${DESIGN_NAME}.mapped.timing.rpt
report_power  > reports/${DESIGN_NAME}.mapped.power.rpt
report_qor > reports/${DESIGN_NAME}.mapped.qor.rpt
report_area -physical -nosplit > reports/${DESIGN_NAME}.mapped.area.rpt
report_area -physical -nosplit -hierarchy > reports/${DESIGN_NAME}.mapped.area_h.rpt
#report_area -nosplit > reports/${DESIGN_NAME}.mapped.area.rpt
#report_area -nosplit -hierarchy > reports/${DESIGN_NAME}.mapped.area_h.rpt
report_hierarchy -full > reports/${DESIGN_NAME}.mapped.hier.rpt
report_clock_gating -nosplit > reports/${DESIGN_NAME}.mapped.clk_gate.rpt

if { [file exists ${SYN_SAIF_FILE}] } {
  report_saif -hier > reports/${DESIGN_NAME}.mapped.saif.rpt
}

write_milkyway -overwrite -output ${DCRM_FINAL_MW_CEL_NAME}


exit
