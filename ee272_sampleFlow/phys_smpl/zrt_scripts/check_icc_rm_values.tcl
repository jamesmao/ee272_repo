##########################################################################################
# Version: F-2011.09 (September 26, 2011)
# Copyright (C) 2007-2011 Synopsys, Inc. All rights reserved.
##########################################################################################

set rm_var_err false



if {$rm_var_err} {
   echo "SCRIPT-Error: ICC-RM variable value error detected.  Exiting ICC."
   exit 0
}
