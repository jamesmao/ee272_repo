#!/bin/tcsh

if ( -f /usr/bin/gcc-4.4 ) then
setenv J_CC gcc-4.4
else
setenv J_CC gcc
endif

if ( -f /hd/cad/modules/tcl/init/csh ) then
source /hd/cad/modules/tcl/init/csh
endif

if ( -f /usr/class/ee/modules/tcl/init/tcsh ) then
source /usr/class/ee/modules/tcl/init/tcsh
else
if ( -f /usr/class/ee/modules/init_modules.csh ) then
source /usr/class/ee/modules/init_modules.csh
endif
endif

#Digital Flow
module load base
module load genesis2
module load vcs-mx
module load dc_shell
module load synopsys_edk
module load icc
module load pts
module load starrc
#module load fm
#module load hercules

#Analog Tools
module load hspice
module load cdesigner
#module load ncx

#module load cx
#module load matlab --> matlab is already in your path...
