#Matlab
alias matlab="/hd/cad/mathworks/matlab.r2009b/bin/matlab"

#Tensilica
#alias xplorer="/hd/cad/tensilica2/xtensa/Xplorer-4.0.1/xplorer"
#JJ_EXTENSA_TOOLS_ROOT=/hd/cad/tensilica2/xtensa/XtDevTools/install/tools/RD-2011.1-linux/XtensaTools
#PATH=$JJ_EXTENSA_TOOLS_ROOT/bin:$PATH
#PATH=$JJ_EXTENSA_TOOLS_ROOT/TIE/bin:$PATH 


# DC SHELL
PATH=/hd/cad/synopsys/dc_shell/F-2011.09-SP4/bin/:$PATH
export SNPSLMD_LICENSE_FILE=26585@vlsi,26585@omni,26585@shimbala:27000@cadlic0
export SYNOPSYS=/hd/cad/synopsys/dc_shell/F-2011.09-SP4/

export SNPSLMD_QUEUE=true
export SNPS_MAX_WAITTIME=7200
export SNPS_MAX_QUEUETIME=7200


# ICC Shell
PATH=:/hd/cad/synopsys/icc/2011.09-SP1/bin/:$PATH
PATH=:/hd/cad/synopsys/pts/2009.12-SP2/bin/:$PATH
PATH=:/hd/cad/synopsys/starrc/F-2011.12-SP2/amd64/bin/:$PATH


#
# VCS
export VCS_ARCH_OVERRIDE=linux
export VCS_HOME=/hd/cad/synopsys/vcs-mx/2010.06
export PATH=${VCS_HOME}/bin:${PATH}
export VIRSIM_HOME=${VCS_HOME}/gui/virsim
export DVE=${VCS_HOME}/gui/dve/bin/
export VCS_LIC_EXPIRE_WARNING=5

###To avoid problems in virsim
##export PATH $VIRSIM_HOME/bin:$PATH
##export XKEYSYMDB /usr/share/X11/XKeysymDB




# LICENSE SETTING
export LM_LICENSE_FILE=5280@vlsi:5280@shimbala:5280@omni:27000@cadlic0

#Setup For Genesis
export GENESIS_LIBS="/hd/cad/genesis2/latest/PerlLibs"
PATH=$GENESIS_LIBS/Genesis2:$PATH

#Setup for NUMBERS


if [ -f /usr/bin/gcc-4.4 ]; then
export J_CC=gcc-4.4
else
export J_CC=gcc
fi
