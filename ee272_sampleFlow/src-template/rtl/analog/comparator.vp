/* *****************************************************************************
 * File: comparator.vp
 * Author: Ofer Shacham
 * 
 * Description:
 * This is an analog functional model of a comparator
 * 
 * REQUIRED GENESIS PARAMETERS:
 * ----------------------------
 * 
 * Change bar:
 * -----------
 * Date          Author   Description
 * Mar 31, 2010  shacham  init version  --  
 * Apr 18, 2012  shacham  clean up of names and genesis syntactic sugar
 * 
 * ****************************************************************************/
//; include ("analog_defs.vph");
module `mname`
  (
   `$input_real` Iref,   // comparator current reference		      
   `$input_real` pos_in,        // positive input
   `$input_real` neg_in,        // negative input
    input [2:0] cfg_offset,
    input Reset,	
    output reg 	out             // output
   );
   timeunit 1ps;
   timeprecision 1ps;
   

  `parameter_real('delay','121e-12')`;   // delay
  `parameter_real('offsetStep','2e-3')`; // step size of offset config
  `parameter_real('offset','8e-3')`;     // offset required in comparator to detect pos_in=neg_in
   integer 	offsetConfig;
   reg 		comp;

   assign 	offsetConfig = cfg_offset;
   
   always @(pos_in, neg_in, Reset) begin
      if (pos_in - neg_in > (offsetStep*offsetConfig - offset)) begin
	 comp <= 1'b1;
      end
      else begin
	 comp <= 1'b0;
      end
   end
   
   always @(comp) out <= #(delay/1e-12) comp;
endmodule // `mname`
