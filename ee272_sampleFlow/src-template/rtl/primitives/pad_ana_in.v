
// This is a wrapper for an analog pad module

module pad_ana_in(
	       input fromboard,
	       output tochip
	       );
   assign tochip = fromboard;
endmodule // pad_dig_in

