// This is a wrapper for an analog pad module

module pad_ana_out(
	       input fromchip,
	       output toboard
	       );
   assign toboard = fromchip;
endmodule // pad_dig_out

