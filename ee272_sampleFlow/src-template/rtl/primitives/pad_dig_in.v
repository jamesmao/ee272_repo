
// This is a wrapper for a digital pad module

module pad_dig_in(
	       input fromboard,
	       output tochip
	       );
   assign tochip = fromboard;
endmodule // pad_dig_in

