// This is a wrapper for a digital pad module

module pad_dig_out(
	       input fromchip,
	       output toboard
	       );
   assign toboard = fromchip;
endmodule // pad_dig_out

